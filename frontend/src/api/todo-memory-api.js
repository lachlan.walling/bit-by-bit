import { v4 as uuidv4 } from 'uuid'
import TodoApi from './todo-api'

class TodoMemoryApi extends TodoApi {
  constructor() {
    super()
    this.todos = []
  }

  async createTodo(name) {
    console.log('Adding todo')
    return {
      status: 'success',
      data: {
        id: uuidv4(),
        name,
        completed: false,
      },
    }
  }

  async deleteTodo(id) {
    this.todos = this.todos.filter((todo) => todo.id !== id)
    return { status: 'success' }
  }

  async setTodoName(id, name) {
    this.todos = this.todos.map((todo) =>
      todo.id === id ? { ...todo, name } : todo,
    )
    return { status: 'success' }
  }

  async setTodoStatus(id, status) {
    this.todos = this.todos.map((todo) =>
      todo.id === id ? { ...todo, completed: status } : todo,
    )
    return { status: 'success' }
  }

  async getTodos() {
    return {
      status: 'success',
      data: this.todos,
    }
  }

  async login(username, password) {
    return {
      status: 'success',
    }
  }

  async logout() {
    return {
      status: 'success',
    }
  }

  async register(username, email, password) {
    return {
      status: 'success',
    }
  }
}

export default TodoMemoryApi

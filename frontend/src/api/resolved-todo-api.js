import TodoHttpApi from './todo-http-api'
import TodoMemoryApi from './todo-memory-api'

let api;
switch (process.env.REACT_APP_API) {
  case 'http':
    api = new TodoHttpApi();
    break;
  case 'memory':
    api = new TodoMemoryApi();
    break;
  default:
    console.error('Unknown api type')
}

export default api
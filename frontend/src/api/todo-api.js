class TodoApi {
  async createTodo(name) {}
  async deleteTodo(id) {}
  async setTodoName(id, name) {}
  async setTodoStatus(id, status) {}
  async getTodos() {}
  async login(username, password) {}
  async register(username, email, password) {}
  async logout() {}
}

export default TodoApi

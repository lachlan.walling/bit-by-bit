import TodoApi from './todo-api'

function success(data) {
  return {
    status: 'success',
    data,
  }
}

async function fail(response) {
  const jsonResponse = await response.json()
  if (response.status === 403) {
    return { status: 'notLoggedIn', error: jsonResponse?.error }
  }
  return { status: 'fail', error: jsonResponse?.error }
}

async function translateHttpResponse(response) {
  if (response.status === 200) {
    return success(await response.json())
  }
  return fail(response)
}

class TodoHttpApi extends TodoApi {
  constructor() {
    super()

    this.url = process.env.REACT_APP_BACKEND_URL

    if (this.url === undefined) {
      console.error('environment variable REACT_APP_BACKEND_URL is undefined')
    } else if (this.url[this.url.length - 1] !== '/') {
      this.url += '/'
    }
  }

  async fetchJson(route, method = 'GET', body = undefined) {
    if (route[0] === '/') {
      route = route.slice(1)
    }
    try {
      return await fetch(
        new Request(this.url + route, {
          method,
          ...(body === undefined
            ? {}
            : {
                body: JSON.stringify(body),
                headers: { 'Content-Type': 'application/json' },
              }),
        }),
        { credentials: 'include' },
      )
    } catch (e) {
      console.log('Exception on fetch ', e)
      throw e
    }
  }

  async useHalLink(todo, relation, data) {
    const link = todo._links[relation]
    return await this.fetchJson(link.href, link.method, data)
  }

  async createTodo(name) {
    const response = await this.fetchJson('todos', 'POST', { name })
    return await translateHttpResponse(response)
  }

  async deleteTodo(todo) {
    const response = await this.useHalLink(todo, 'delete')
    return await translateHttpResponse(response)
  }

  async setTodoStatus(todo, completed) {
    const response = await this.useHalLink(todo, 'update', { completed })
    return await translateHttpResponse(response)
  }

  async setTodoName(todo, name) {
    console.log(todo, name)
    const response = await this.useHalLink(todo, 'update', { name })
    return await translateHttpResponse(response)
  }

  async getTodos() {
    const response = await this.fetchJson(`todos`)
    if (response.status === 200) {
      const hal = await response.json()
      return success(hal.items)
    }
    return fail(response)
  }

  async login(username, password) {
    const response = await this.fetchJson(`login`, 'POST', {
      username,
      password,
    })
    if (response.status === 200) {
      return success({})
    }
    return await fail(response)
  }

  async register(username, email, password) {
    const response = await this.fetchJson(`register`, 'POST', {
      username,
      email,
      password,
    })
    if (response.status === 200) {
      return success({})
    }
    return await fail(response)
  }

  async logout() {
    const response = await this.fetchJson(`logout`, 'POST', {})
    if (response.status === 200) {
      return success({})
    }
    return await fail(response)
  }
}

export default TodoHttpApi

import UserContext from './UserContext'
import resolvedTodoApi from '../api/resolved-todo-api'
import TodoApi from '../api/todo-api'
import {useContext} from 'react'
import { useNavigate } from 'react-router-dom'

export default  () => {
  const userContext = useContext(UserContext)
  const navigate = useNavigate();

  const apiWrapper = (func) => async (...args) => {
    const response = await func.apply(resolvedTodoApi, args)

    if (response.status === 'notLoggedIn') {
      userContext.setLoggedIn(false)
      navigate('/login')
    }

    return response
  }

  const wrapped = Object.getOwnPropertyNames(TodoApi.prototype)
    .filter((funcName) => funcName !== 'constructor')
    .reduce((accumulatedFunctions, funcName) => ({
      ...accumulatedFunctions,
      [funcName]: apiWrapper(resolvedTodoApi[funcName])
    }), {})

  return {
    ...wrapped,
    async login(username, password) {
      const response = await resolvedTodoApi.login(username, password)
      if (response.status === 'success') {
        userContext.setLoggedIn(true)
      }
      return response
    },
    async logout() {
      const response = await resolvedTodoApi.logout()
      if (response.status === 'success')
        userContext.setLoggedIn(false)
      return response
    },
    async register(username, email, password) {
      const response = await resolvedTodoApi.register(username, email, password)
      if (response.status === 'success')
        userContext.setLoggedIn(true)
      return response
    },
    getTodos: apiWrapper(async () => {
      const response = await resolvedTodoApi.getTodos()
      if (response.status === 'success') {
        userContext.setLoggedIn(true)
      }
      return response
    }),
    loggedIn: userContext.loggedIn
  }
}
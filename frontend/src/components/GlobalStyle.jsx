import { createGlobalStyle } from 'styled-components'
import { COLOURS } from '../constants'

const GlobalStyle = createGlobalStyle`
  *, *::before, *::after {
      box-sizing: border-box;
  }
  html, body, #root {
      height: 100%;
      color: ${COLOURS.foreground};
      background-color: ${COLOURS.background};
      user-select: none;
      -moz-user-select: none;
      -webkit-user-select: none;
      -ms-user-select: none;
  }
  body {
    margin: 0;
    padding: 0;
    line-height: 1.4;
  }
`

export default GlobalStyle
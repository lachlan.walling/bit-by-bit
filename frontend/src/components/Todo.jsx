import * as React from 'react'
import styled from 'styled-components'
import Card from './Base/Card'
import Expander from './Base/Expander'
import Checkbox from './Base/Checkbox'
import PropTypes from 'prop-types'
import Button from './Base/Button'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencil, faTrash } from '@fortawesome/free-solid-svg-icons'
import TextField from './Base/TextField'
import Selectable from './Base/Selectable'
import { SIZES } from '../constants'

const TodoName = styled.span`
  text-decoration: ${({ isComplete }) =>
    isComplete ? 'line-through' : 'none'};
  font-size: ${SIZES.medium};
`

const Todo = ({
  todo,
  deleteTodo,
  setTodoStatus,
  isEditing,
  setIsEditing,
  setTodoName,
}) => {
  const onSubmit = (newName) => {
    setTodoName(todo, newName)
    setIsEditing(false)
  }
  const editTodo = () => {
    setIsEditing(true)
  }
  return (
    <li>
      <FlexWrapper>
        <Checkbox
          value={todo.completed}
          onChange={(v) => setTodoStatus(todo, v)}
        />
        {isEditing ? (
          <TextField
            initialValue={todo.name}
            onSubmit={onSubmit}
            autoFocus={true}
          />
        ) : (
          <Selectable>
            <TodoName isComplete={todo.completed}>{todo.name}</TodoName>
          </Selectable>
        )}
        <Expander />
        <Button onClick={() => editTodo()}>
          <FontAwesomeIcon icon={faPencil} />
        </Button>
        <Button onClick={() => deleteTodo(todo)}>
          <FontAwesomeIcon icon={faTrash} />
        </Button>
      </FlexWrapper>
    </li>
  )
}

Todo.propTypes = {
  todo: PropTypes.shape({
    id: PropTypes.string.isRequired,
    completed: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
  deleteTodo: PropTypes.func.isRequired,
  setTodoStatus: PropTypes.func.isRequired,
  setTodoName: PropTypes.func.isRequired,
  setIsEditing: PropTypes.func.isRequired,
  isEditing: PropTypes.bool,
}

Todo.defaultProps = {
  isEditing: false,
}

const FlexWrapper = styled(Card)`
  display: flex;
  min-height: 10px;
  align-items: baseline;
  gap: 10px;
`

export default Todo

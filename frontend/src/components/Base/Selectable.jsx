import * as React from 'react'
import styled from 'styled-components'

const Selectable = ({children}) => {
  return (
    <Wrapper>
      {children}
    </Wrapper>
  );
}

const Wrapper = styled.div`
  user-select: text;
  -moz-user-select: text;
  -webkit-user-select: text;
  -ms-user-select: text;
`

export default Selectable;
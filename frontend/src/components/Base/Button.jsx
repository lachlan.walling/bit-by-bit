import * as React from 'react'
import styled from 'styled-components'
import { COLOURS, SIZES } from '../../constants'
import PropTypes from 'prop-types';

const Button = ({onClick, children, className}) => {
    return (
        <Wrapper className={className} onClick={onClick}>
            {children}
        </Wrapper>
    )
}

Button.propTypes = {
    onClick: PropTypes.func,
    children: PropTypes.node
}

Button.defaultProps = {
    onClick: ()=>{}
}

const Wrapper = styled.button`
background-color: ${COLOURS.darkPurple};
color: ${COLOURS.background};
:focus {
    outline-color: ${COLOURS.darkPurple}
}
:hover {
    background-color: ${COLOURS.purple}
}
border: 2px solid transparent;
:focus {
    outline-offset: 2px;
}
// TODO potential issue with collapse?
margin: 4px;
font-size: ${SIZES.medium};
`

export default Button;
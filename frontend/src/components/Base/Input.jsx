import * as React from 'react'
import styled from 'styled-components'
import { SIZES } from '../../constants'
import PropTypes from 'prop-types'

const Wrapper = styled.input`
  // TODO potential issue with collapse?
  margin: 4px;
  font-size: ${SIZES.medium};
`

const Input = ({
  className,
  placeholder,
  type,
  value,
  setValue,
  onBlur,
  autoFocus,
  onEnter,
}) => {
  return (
    <Wrapper
      type={type}
      placeholder={placeholder}
      className={className}
      value={value}
      onChange={({ target: { value } }) => setValue(value)}
      onBlur={onBlur}
      autoFocus={autoFocus}
      onKeyDown={(event) => event.key === 'Enter' && onEnter()}
    />
  )
}

Input.propTypes = {
  value: PropTypes.string.isRequired,
  setValue: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string,
  className: PropTypes.string,
  onBlur: PropTypes.func,
  autoFocus: PropTypes.bool,
  onEnter: PropTypes.func,
}

Input.defaultProps = {
  type: 'text',
  placeholder: '',
  onBlur: () => {},
  autoFocus: false,
  onEnter: () => {},
}

export default Input

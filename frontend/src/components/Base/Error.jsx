import * as React from 'react'
import styled from 'styled-components'

const Wrapper = styled.p`
  height: 1rem
`

const Error = ({message, className}) => {
  return <Wrapper className={className}>{message}</Wrapper>
}

export default Error
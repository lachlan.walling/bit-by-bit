import { useState } from 'react'
import PropTypes from 'prop-types'
import Input from './Input'

const TextField = ({ className, initialValue, onSubmit, autoFocus }) => {
  const [value, setValue] = useState(initialValue)
  return (
    <Input
      className={className}
      value={value}
      setValue={setValue}
      onBlur={() => onSubmit(value)}
      onEnter={() => onSubmit(value)}
      autoFocus={autoFocus}
    />
  )
}

TextField.propTypes = {
  initialValue: PropTypes.string,
  className: PropTypes.string,
  onSubmit: PropTypes.func.isRequired,
  autoFocus: PropTypes.bool,
}

TextField.defaultProps = {
  initialValue: '',
  autoFocus: false,
}

export default TextField

import * as React from 'react'
import PropTypes from 'prop-types';

const Checkbox = ({value, onChange}) => {
    const [isChecked, setChecked] = React.useState(value)
    const handleChange = ({target: {checked}}) => {
        onChange(checked)
        setChecked(checked)
    }
    return <input 
        type="checkbox" 
        checked={isChecked} 
        onChange={handleChange}
    />
}

Checkbox.propTypes = {
    value: PropTypes.bool,
    onChange: PropTypes.func.isRequired
}

Checkbox.defaultProps = {
    initialValue: false,
}

export default Checkbox;
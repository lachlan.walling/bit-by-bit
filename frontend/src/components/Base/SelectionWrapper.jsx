import * as React from 'react'
import PropTypes from 'prop-types';
import styled from 'styled-components'
import {COLOURS} from '../../constants'

const SelectionWrapper = ({select, isSelected, children}) => {
  function onClick(event)  {
    event.stopPropagation()
    select()
  }
  return (
  <Wrapper onClick={onClick} isSelected={isSelected}>
    {children}
  </Wrapper>
  )
}

const Wrapper=styled.div`
    outline: ${({isSelected})=>isSelected() ? 'solid': 'none'};
    outline-width: 2px;
    outline-color: ${COLOURS.purple};
`


SelectionWrapper.propTypes = {
  onClick: PropTypes.func,
  isSelected: PropTypes.func
}

SelectionWrapper.defaultProps = {
  onClick: () => {},
  isSelected: () => false
}


export default SelectionWrapper

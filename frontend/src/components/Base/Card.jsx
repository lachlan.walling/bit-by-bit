import styled from 'styled-components'
import {COLOURS} from '../../constants'

const Card = styled.div`
    background-color: ${COLOURS.cardBackground};
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    padding: 8px;
`

export default Card;
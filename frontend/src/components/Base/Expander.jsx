import styled from 'styled-components'

const Expander = styled.div`
    flex: 1;
`
export default Expander;
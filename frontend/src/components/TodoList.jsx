import * as React from 'react'
import Todo from './Todo'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Card from './Base/Card'
import { SIZES } from '../constants'
import SelectionWrapper from './Base/SelectionWrapper'

const TodoList = ({
  todos,
  deleteTodo,
  setTodoName,
  setTodoStatus,
  selectTodo,
  selectedTodoId,
  isEditing,
  setIsEditing,
}) => {
  return (
    <Wrapper>
      {todos.length > 0 ? (
        todos.map((todo) => (
          <SelectionWrapper
            key={todo.id}
            select={() => selectTodo(todo.id)}
            isSelected={() => todo.id === selectedTodoId}
          >
            <Todo
              todo={todo}
              deleteTodo={deleteTodo}
              setTodoStatus={setTodoStatus}
              setTodoName={setTodoName}
              setIsEditing={setIsEditing}
              isEditing={isEditing && todo.id === selectedTodoId}
            />
          </SelectionWrapper>
        ))
      ) : (
        <Message>Todo list empty</Message>
      )}
    </Wrapper>
  )
}

TodoList.propTypes = {
  todos: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      completed: PropTypes.bool.isRequired,
    }),
  ),
  deleteTodo: PropTypes.func.isRequired,
  setTodoName: PropTypes.func.isRequired,
  setTodoStatus: PropTypes.func.isRequired,
  selectTodo: PropTypes.func,
  selectedTodoId: PropTypes.string,
  isEditing: PropTypes.bool,
  setIsEditing: PropTypes.func.isRequired,
}

TodoList.defaultProps = {
  todos: [],
  selectTodo: () => {},
  selectedTodoId: null,
  isEditing: false,
}

const Message = styled(Card)`
  font-size: ${SIZES.medium};
`

const Wrapper = styled.ul`
  list-style-type: none;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  gap: 8px;
  flex: 1;
  flex-basis: 250px;
  margin: 0;
  padding: 4px;
`

export default TodoList

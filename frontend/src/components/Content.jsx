import styled from 'styled-components'

const Content = styled.div`
  display: flex;
  height: 100%;
  gap: 5px;
  align-items: baseline;
`

export default Content;
import TodosPage from './Pages/TodosPage'
import HomePage from './Pages/HomePage'
import LoginPage from './Pages/LoginPage'
import RegistrationPage from './Pages/RegistrationPage'
import UserContext from './UserContext'

import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import * as React from 'react'
import { useState } from 'react'

const router = createBrowserRouter([
  {
    path: "/todos",
    element: <TodosPage/>,
  },
  {
    path: "/",
    element: <HomePage/>
  },
  {
    path: "/login",
    element: <LoginPage/>,
  },
  {
    path: "/register",
    element: <RegistrationPage/>,
  }
]);

const App = () => {
  let [loggedIn, setLoggedIn] = useState(false)
  return (
    <UserContext.Provider value={{loggedIn, setLoggedIn}}>
      <RouterProvider router={router}/>
    </UserContext.Provider>
    )
}

export default App;
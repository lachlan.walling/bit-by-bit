import * as React from 'react'
import { useState } from 'react'
import withPageTemplate from './PageTemplate'
import styled from 'styled-components'
import Input from '../Base/Input'
import Button from '../Base/Button'
import Error from '../Base/Error'
import Expander from '../Base/Expander'
import { useNavigate } from 'react-router-dom'
import useApi from '../useApi'

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  height: 100%;
  gap: 5px;
`

function RegistrationForm() {
  const [username, setUsername] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const navigate = useNavigate()
  const todoApi = useApi()

  const submitRegistration = async (event) => {
    event.preventDefault()
    const result = await todoApi.register(username, email, password)
    if (result.status === 'success') {
      navigate('/todos')
    } else {
      setErrorMessage(result.error)
    }
  }

  return (
    <StyledForm method="post" action="/register">
      <Error message={errorMessage} />
      <Input
        type="text"
        name="username"
        placeholder="Username"
        value={username}
        setValue={setUsername}
      />
      <Input
        type="email"
        name="email"
        placeholder="Email"
        value={email}
        setValue={setEmail}
      />
      <Input
        type="password"
        name="password"
        placeholder="Password"
        value={password}
        setValue={setPassword}
      />
      <Button type="submit" onClick={submitRegistration}>
        Register
      </Button>
    </StyledForm>
  )
}

const RegistrationPage = () => {
  return (
    <>
      <Expander />
      <RegistrationForm />
      <Expander />
    </>
  )
}

export default withPageTemplate(RegistrationPage)

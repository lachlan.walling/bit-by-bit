import TodoList from '../TodoList'
import Expander from '../Base/Expander'
import styled from 'styled-components'
import { DEVICE_BREAKPOINT } from '../../constants'

import { useEffect, useState } from 'react'
import Button from '../Base/Button'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import withPageTemplate from './PageTemplate'

import useApi from '../useApi'
import { faPlus } from '@fortawesome/free-solid-svg-icons'

const CreateButton = styled(Button)`
  @media (max-width: ${DEVICE_BREAKPOINT}) {
    position: fixed;
    bottom: 16px;
    right: 16px;
    border-radius: 5px;
    width: 2.2rem;
    height: 2.2rem;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  }
`

const TodosPage = ({ setHeaderContent }) => {
  const [todos, setTodos] = useState([])
  const [loaded, setLoaded] = useState(false)
  const [selectedTodoId, selectTodo] = useState(null)
  const [isEditing, setIsEditing] = useState(false)
  const api = useApi()

  const addTodo = async (name) => {
    const response = await api.createTodo(name)
    if (response.status === 'success') {
      setTodos((currentTodos) => [...currentTodos, response.data])
      selectTodo(response.data.id)
      setIsEditing(true)
    }
  }

  const deleteTodo = async (todo) => {
    const response = await api.deleteTodo(todo)
    if (response.status === 'success') {
      if (selectedTodoId === todo.id) selectTodo(null)
      setTodos((currentTodos) =>
        currentTodos.filter((filterTodo) => filterTodo.id !== todo.id),
      )
    }
  }

  const setTodoStatus = async (todo, status) => {
    const response = await api.setTodoStatus(todo, status)
    if (response.status === 'success') {
      setTodos((currentTodos) =>
        currentTodos.map((mapTodo) =>
          mapTodo.id === todo.id ? { ...mapTodo, completed: status } : mapTodo,
        ),
      )
    }
  }

  const setTodoName = async (todo, name) => {
    const response = await api.setTodoName(todo, name)
    if (response.status === 'success') {
      setTodos((currentTodos) =>
        currentTodos.map((mapTodo) =>
          mapTodo.id === todo.id ? { ...mapTodo, name: name } : mapTodo,
        ),
      )
    }
  }

  const getTodos = async () => {
    const response = await api.getTodos()
    if (response.status === 'success') {
      setTodos(response.data)
    }
  }

  useEffect(() => {
    ;(async () => {
      await getTodos()
      setHeaderContent(
        <>
          <CreateButton onClick={() => addTodo('New todo')}>
            <FontAwesomeIcon icon={faPlus} />
          </CreateButton>
        </>,
      )

      setLoaded(true)
    })()
  }, [])

  return (
    <>
      <Expander />
      {loaded ? (
        <TodoList
          todos={todos}
          deleteTodo={deleteTodo}
          setTodoStatus={setTodoStatus}
          setTodoName={setTodoName}
          selectTodo={selectTodo}
          selectedTodoId={selectedTodoId}
          isEditing={isEditing}
          setIsEditing={setIsEditing}
        />
      ) : null}
      <Expander />
    </>
  )
}

export default withPageTemplate(TodosPage)

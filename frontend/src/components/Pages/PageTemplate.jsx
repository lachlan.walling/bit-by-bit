import GlobalStyle from '../GlobalStyle'
import Header from '../Header'
import Content from '../Content'
import * as React from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { faRightToBracket } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Button from '../Base/Button'
import useApi from '../useApi'

const withPageTemplate = (Component) => (props) => {
  const [headerContent, setHeaderContent] = useState(undefined)
  const navigate = useNavigate()
  const todoApi = useApi()

  const login = async () => {
    navigate('/login')
  }
  const logout = async () => {
    await todoApi.logout()
    navigate('/')
  }
  return (
    <>
      <GlobalStyle />
      <Header>
        {headerContent}
        <Button onClick={todoApi.loggedIn ? logout : login}>
          <FontAwesomeIcon icon={faRightToBracket} />
        </Button>
      </Header>
      <Content>
        <Component {...props} setHeaderContent={setHeaderContent} />
      </Content>
    </>
  )
}

export default withPageTemplate

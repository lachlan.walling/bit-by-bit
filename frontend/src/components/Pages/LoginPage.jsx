import * as React from 'react'
import { useState } from 'react'
import withPageTemplate from './PageTemplate'
import Expander from '../Base/Expander'
import Button from '../Base/Button'
import Error from '../Base/Error'
import styled from 'styled-components'
import Input from '../Base/Input'
import { Link, useNavigate } from 'react-router-dom'
import useApi from '../useApi'

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
  height: 100%;
  gap: 5px;
  align-items: center;
`

function LoginForm() {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  const navigate = useNavigate()
  const todoApi = useApi()

  const submitLogin = async (event) => {
    event.preventDefault()
    const result = await todoApi.login(username, password)
    if (result.status === 'success') {
      navigate('/todos')
    } else {
      setErrorMessage(result.error)
    }
  }

  return (
    <StyledForm method="post" action="/login">
      <Error message={errorMessage} />
      <Link to="/register"> Register </Link>
      <Input
        type="text"
        name="username"
        placeholder="Username"
        value={username}
        setValue={setUsername}
      />
      <Input
        type="password"
        name="password"
        placeholder="Password"
        value={password}
        setValue={setPassword}
      />
      <Button type="submit" onClick={submitLogin}>
        Login
      </Button>
    </StyledForm>
  )
}

const LoginPage = withPageTemplate(() => {
  return (
    <>
      <Expander />
      <LoginForm />
      <Expander />
    </>
  )
})

export default LoginPage

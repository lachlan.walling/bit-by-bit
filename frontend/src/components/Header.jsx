import * as React from 'react'
import styled from 'styled-components'
import {COLOURS} from '../constants';
import Expander from './Base/Expander'

const Header = ({children}) => {
  return (
    <Wrapper>
      <h1>Todos</h1>
      <Expander/>
      {children}
    </Wrapper>
  );
}

const Wrapper = styled.header`
  display: flex;
  flex-direction: row;
  background-color: ${COLOURS.darkPurple};
  color: ${COLOURS.background};
  padding-left: 20px;
  padding-right: 20px;
  h1 {
    margin: 0;
  }
`

export default Header;
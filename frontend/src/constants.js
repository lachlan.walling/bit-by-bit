export const COLOURS = {
  white: 'hsl(0deg, 0%, 100%)',
  gray: {
    100: 'hsl(185deg, 5%, 95%)',
    300: 'hsl(190deg, 5%, 80%)',
    500: 'hsl(196deg, 4%, 60%)',
    700: 'hsl(220deg, 5%, 40%)',
    900: 'hsl(220deg, 3%, 20%)',
  },

  background: 'hsl(231deg, 15%, 18%)',
  sidebarBackground: 'hsl(212deg, 14%, 31%)',
  cardBackground: 'hsl(232deg, 14%, 31%)',
  foreground: 'hsl(60deg, 30%, 96%)',

  cyan: 'hsl(191deg, 97%, 77%)',
  green: 'hsl(135deg, 94%, 65%)',
  orange: 'hsl(31deg, 100%, 71%)',
  pink: 'hsl(326deg, 100%, 74%)',
  purple: 'hsl(265deg, 89%, 78%)',
  red: 'hsl(0deg, 100%, 67%)',
  yellow: 'hsl(65deg, 92%, 76%)',

  darkCyan: 'hsl(191deg, 76% 69%)',
  darkGreen: 'hsl(135deg, 77%, 58%)',
  darkOrange: 'hsl(31deg, 83%, 63%)',
  darkPink: 'hsl(326eg, 72%, 61%)',
  darkPurple: 'hsl(265deg, 64%, 65%)',
  darkRed: 'hsl(0deg, 76%, 59%)',
  darkYellow: 'hsl(65deg, 88%, 62%)',
}

export const SIZES = {
  small: `${16 / 16}rem`,
  medium: `${20 / 16}rem`,
  large: `${24 / 16}rem`,
}

export const DEVICE_BREAKPOINT = '700px'
#!/bin/bash
set -exo pipefail

#TODO docker build

aws cloudformation deploy \
  --template-file cloudformation-templates/ecr.cfn.template.yml \
  --stack-name aws-workshop-04-ecr \
  --profile j1-pf-sandbox-admin \
  --parameter-overrides CallingUserArn="$(aws sts get-caller-identity --profile j1-pf-sandbox-admin | jq -r .Arn)";

WORKSHOP_REPOSITORY_URI=$(aws cloudformation describe-stacks \
    --stack-name aws-workshop-04-ecr \
    --profile j1-pf-sandbox-developer \
    --output text \
    --query 'Stacks[0].Outputs[?OutputKey==`RepositoryUri`].OutputValue'); \
echo ${WORKSHOP_REPOSITORY_URI};

aws ecr get-login-password  \
  --profile j1-pf-sandbox-developer | \
  docker login --username AWS --password-stdin ${WORKSHOP_REPOSITORY_URI};

docker tag bit-by-bit:latest ${WORKSHOP_REPOSITORY_URI};
docker push ${WORKSHOP_REPOSITORY_URI};

aws cloudformation deploy \
  --template-file cloudformation-templates/vpc.cfn.template.yml \
  --stack-name aws-workshop-04-vpc \
  --profile j1-pf-sandbox-developer \
  --parameter-overrides Tag="aws-workshop-04-vpc";

WORKSHOP_PUBLIC_SUBNET_ID=$(aws cloudformation describe-stacks \
    --stack-name aws-workshop-04-vpc \
    --profile j1-pf-sandbox-developer \
    --output text \
    --query 'Stacks[0].Outputs[?OutputKey==`PublicSubnetId`].OutputValue'); \
echo ${WORKSHOP_PUBLIC_SUBNET_ID};

WORKSHOP_VPC_ID=$(aws cloudformation describe-stacks \
    --stack-name aws-workshop-04-vpc \
    --profile j1-pf-sandbox-developer \
    --output text \
    --query 'Stacks[0].Outputs[?OutputKey==`VpcId`].OutputValue'); \
echo ${WORKSHOP_VPC_ID};

aws cloudformation deploy  \
  --template-file cloudformation-templates/ecs.cfn.template.yml  \
  --stack-name aws-workshop-04-ecs \
  --profile j1-pf-sandbox-admin \
  --capabilities CAPABILITY_NAMED_IAM \
  --parameter-overrides \
    VpcId="${WORKSHOP_VPC_ID}" \
    SubnetId="${WORKSHOP_PUBLIC_SUBNET_ID}" \
    RepositoryUri="${WORKSHOP_REPOSITORY_URI}";

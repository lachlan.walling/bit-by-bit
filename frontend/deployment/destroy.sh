#!/bin/bash
set -exo pipefail 

WORKSHOP_IMAGE_DIGEST="$(aws ecr list-images \
  --output text \
  --repository-name workshop-repository \
  --query 'imageIds[0].imageDigest' \
  --profile j1-pf-sandbox-developer)"; 

echo "WORKSHOP_IMAGE_DIGEST = ${WORKSHOP_IMAGE_DIGEST}";


aws ecr batch-delete-image \
     --repository-name workshop-repository \
     --profile j1-pf-sandbox-developer \
     --image-ids imageDigest=${WORKSHOP_IMAGE_DIGEST};

aws cloudformation delete-stack  \
  --stack-name aws-workshop-04-ecr \
  --profile j1-pf-sandbox-admin;

aws cloudformation delete-stack  \
  --stack-name aws-workshop-04-ecs \
  --profile j1-pf-sandbox-admin;

aws cloudformation delete-stack  \
  --stack-name aws-workshop-04-vpc \
  --profile j1-pf-sandbox-admin;

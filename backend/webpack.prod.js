const { merge } = require('webpack-merge')
const WebpackCommon = require('./webpack.common')

module.exports = merge(WebpackCommon, {
  mode: 'production',
})

import Cookies from 'cookies'

class TestSession {
  constructor() {
    this.session = { userId: undefined }
    this.middleware = this.middleware.bind(this)
  }

  logInTest(userId) {
    this.session.userId = userId
  }

  logOutTest() {
    this.session.userId = undefined
  }

  isLoggedIn() {
    return this.session.userId !== undefined
  }

  middleware(req, res, next) {
    var cookies = new Cookies(req, res)
    req.session = this.session

    var writeHead = res.writeHead
    res.writeHead = (...args) => {
      cookies.set('session', this.session.userId || '')
      return writeHead.apply(res, args)
    }

    next()
  }
}

module.exports = TestSession

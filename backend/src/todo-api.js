import { DefaultIdGenerator } from './utils'
import InMemoryTodoRepository from './repositories/in-memory-todo-repository'
import InMemoryUserRepository from './repositories/in-memory-user-repository'
import Todo from './todo'
import User from './user'
import validate from 'validate.js'

const StatusCodes = {
  success: 'success',
  todoNotFound: 'todoNotFound',
  missingTodoName: 'missingTodoName',
  emptyUsername: 'emptyUsername',
  invalidEmail: 'invalidEmail',
  emptyPassword: 'emptyPassword',
  badLogin: 'badLogin',
  userNotFound: 'userNotFound',
}

const success = (body) => {
  return {
    status: StatusCodes.success,
    data: body,
  }
}

const failure = (status) => {
  return {
    status,
  }
}

class TodoApi {
  constructor({
    createIdGenerator = DefaultIdGenerator,
    todoRepository = new InMemoryTodoRepository(),
    userRepository = new InMemoryUserRepository(),
  }) {
    this.todoIdGenerator = createIdGenerator()
    this.userIdGenerator = createIdGenerator()
    this.todoRepository = todoRepository
    this.userRepository = userRepository
  }

  async getTodos() {
    return success(await this.todoRepository.getAll())
  }

  async getTodo(id) {
    if (!(await this.todoRepository.hasTodo(id)))
      return failure(StatusCodes.todoNotFound)
    const todo = await this.todoRepository.getById(id)
    return success(todo)
  }

  createTodo(name) {
    return new Todo(this.todoIdGenerator, name)
  }

  createUser(username, email, password) {
    return new User(this.userIdGenerator, username, email, password)
  }

  async addTodo(name) {
    if (name === undefined) return failure(StatusCodes.missingTodoName)

    const todo = this.createTodo(name)
    await this.todoRepository.save(todo)
    return success(todo)
  }

  async deleteTodo(id) {
    if (!(await this.todoRepository.hasTodo(id)))
      return failure(StatusCodes.todoNotFound)

    await this.todoRepository.delete(id)
    return success({})
  }

  async updateTodo(id, changes) {
    if (!(await this.todoRepository.hasTodo(id)))
      return failure(StatusCodes.todoNotFound)

    if (changes === undefined) changes = {}
    const todo = await this.todoRepository.updateTodo(id, changes)
    return success(todo)
  }

  async register(username, email, password) {
    if (typeof username !== 'string' || username === '') {
      return failure(StatusCodes.emptyUsername)
    }
    if (validate.single(email, { presence: true, email: true }) !== undefined) {
      return failure(StatusCodes.invalidEmail)
    }
    if (typeof password !== 'string' || password === '') {
      return failure(StatusCodes.emptyPassword)
    }
    const user = this.createUser(username, email, password)
    await this.userRepository.save(user)
    return success(user.getExternalUser())
  }

  async login(username, password) {
    if (typeof username !== 'string' || username === '') {
      return failure(StatusCodes.emptyUsername)
    }
    if (typeof password !== 'string' || password === '') {
      return failure(StatusCodes.emptyPassword)
    }
    const user = await this.userRepository.findByUsername(username)
    if (user && user.checkPassword(password))
      return success(user.getExternalUser())
    return failure(StatusCodes.badLogin)
  }

  async findUser(id) {
    if (typeof id !== 'string' || id === '') {
      return failure(StatusCodes.emptyUsername)
    }
    const user = await this.userRepository.getById(id)
    if (user) return success(user.getExternalUser())
    return failure(StatusCodes.userNotFound)
  }
}

export default TodoApi

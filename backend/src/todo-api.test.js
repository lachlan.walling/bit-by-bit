import InMemoryTodoRepository from './repositories/in-memory-todo-repository'
import InMemoryUserRepository from './repositories/in-memory-user-repository'
//const MongooseTodoRepository = require('./repositories/mongoose-todo-repository.js')
import TodoApi from './todo-api'
import { CounterIdGenerator } from './utils'
import User from './user'

// TODO unique emails, usernames
// TODO password validation?
import * as matchers from 'jest-extended'

expect.extend(matchers)

// TODO check types
// TODO check invalid keys

const dummyData = [
  { id: '0', name: 'todo 1', completed: true },
  { id: '1', name: 'todo 2', completed: false },
  { id: '2', name: 'todo 3', completed: true },
  { id: '3', name: 'todo 4', completed: false },
]

const setData = async (repository, data) => {
  for (const todo of data) {
    await repository.save(structuredClone(todo))
  }
}

describe('todo-api', () => {
  var api
  var todoRepository
  var userRepository
  beforeEach(async () => {
    todoRepository = new InMemoryTodoRepository()
    userRepository = new InMemoryUserRepository()
    await todoRepository.setup()
    await userRepository.setup()
    api = new TodoApi({
      createIdGenerator: CounterIdGenerator,
      todoRepository,
      userRepository,
    })
  })
  afterEach(async () => {
    await todoRepository.cleanup()
    todoRepository = null
    api = null
  })

  describe('registration', () => {
    it('returns an error when trying to register a user with an empty username', async () => {
      expect(await api.register('', 'someuser@somesite.com', 'god')).toEqual({
        status: 'emptyUsername',
      })
    })
    it('returns an error when trying to register a user with an empty email', async () => {
      expect(await api.register('username', '', 'god')).toEqual({
        status: 'invalidEmail',
      })
    })
    it('returns an error when trying to register a user with an invalid email', async () => {
      expect(await api.register('username', 'notanemail', 'god')).toEqual({
        status: 'invalidEmail',
      })
    })
    it('returns an error when trying to register a user with an empty password', async () => {
      expect(
        await api.register('username', 'someuser@somesite.com', ''),
      ).toEqual({
        status: 'emptyPassword',
      })
    })
    it('registers a user', async () => {
      const response = await api.register(
        'username',
        'someuser@somesite.com',
        'god',
      )
      expect(response).toEqual({
        status: 'success',
        data: {
          id: '0',
          username: 'username',
          email: 'someuser@somesite.com',
        },
      })
      expect(response).not.toHaveProperty('password')
      expect(response).not.toHaveProperty('passwordHash')
      expect(await userRepository.getById('0')).toMatchObject({
        id: '0',
        username: 'username',
        email: 'someuser@somesite.com',
      })
    })
  })

  describe('login', () => {
    let user
    beforeEach(async () => {
      user = new User(
        CounterIdGenerator(),
        'username',
        'someuser@somesite.com',
        'god',
      )
      await userRepository.save(user)
    })
    it('returns an error when trying to login with an empty username', async () => {
      expect(await api.login('', 'god')).toEqual({
        status: 'emptyUsername',
      })
    })
    it('returns an error when trying to login with an empty password', async () => {
      expect(await api.login('username', '')).toEqual({
        status: 'emptyPassword',
      })
    })
    it('returns an error when trying to login with a nonexistent username', async () => {
      expect(await api.login('wronguser', 'god')).toEqual({
        status: 'badLogin',
      })
    })

    it('returns an error when trying to login with the wrong password', async () => {
      expect(await api.login('username', 'badpass')).toEqual({
        status: 'badLogin',
      })
    })
    it('returns the user for a correct username and password', async () => {
      expect(await api.login('username', 'god')).toEqual({
        status: 'success',
        data: {
          email: 'someuser@somesite.com',
          id: '0',
          username: 'username',
        },
      })
    })
  })

  describe('finding user', () => {
    it('returns an error when the user does not exist', async () => {
      expect(await api.findUser('0')).toEqual({
        status: 'userNotFound',
      })
    })

    it('finds a user', async () => {
      let user = new User(
        CounterIdGenerator(),
        'username',
        'someuser@somesite.com',
        'god',
      )
      await userRepository.save(user)
      expect(await api.findUser('0')).toEqual({
        status: 'success',
        data: user.getExternalUser(),
      })
    })
  })

  describe('getting todos', () => {
    it('gets an empty list of todos', async () => {
      expect(await api.getTodos()).toEqual({
        status: 'success',
        data: [],
      })
    })

    it('gets a list of todos', async () => {
      await setData(todoRepository, dummyData)
      const response = await api.getTodos()
      expect(response.status).toEqual('success')
      expect(response.data).toIncludeAllMembers(dummyData)
    })

    it('gets a particular todo', async () => {
      await setData(todoRepository, dummyData)
      expect(await api.getTodo('2')).toEqual({
        status: 'success',
        data: { id: '2', name: 'todo 3', completed: true },
      })
    })
    it('returns an error for a non existent todo', async () => {
      expect(await api.getTodo('0')).toEqual({
        status: 'todoNotFound',
      })
    })
  })

  describe('adding todos', () => {
    it('adds a todo', async () => {
      expect(await api.addTodo('todo')).toEqual({
        status: 'success',
        data: { id: '0', name: 'todo', completed: false },
      })
      expect(await todoRepository.getAll()).toEqual([
        { id: '0', name: 'todo', completed: false },
      ])
    })

    it('returns an error for adding a todo with no name', async () => {
      expect(await api.addTodo()).toEqual({
        status: 'missingTodoName',
      })
      expect(await todoRepository.getAll()).toEqual([])
    })
  })

  describe('deleting todos', () => {
    it('deletes a todo', async () => {
      await setData(todoRepository, dummyData)
      expect(await api.deleteTodo('2')).toEqual({
        status: 'success',
        data: {},
      })
      expect(await todoRepository.getAll()).toEqual(
        dummyData.filter((todo) => todo.id !== '2'),
      )
    })

    it('returns an error for deleting a non-existent todo', async () => {
      expect(await api.deleteTodo('0')).toEqual({
        status: 'todoNotFound',
      })
    })
  })

  describe('updating todos', () => {
    it('updates todo.completed', async () => {
      await setData(todoRepository, dummyData)
      const requestData = { completed: false }
      const newTodo = { id: '2', name: 'todo 3', completed: false }
      expect(await api.updateTodo('2', requestData)).toEqual({
        status: 'success',
        data: newTodo,
      })
      expect(await todoRepository.getAll()).toContainEqual(newTodo)
    })
    it('updates todo.name', async () => {
      await setData(todoRepository, dummyData)
      const requestData = { name: 'new name' }
      const newTodo = { id: '2', name: 'new name', completed: true }
      expect(await api.updateTodo('2', requestData)).toEqual({
        status: 'success',
        data: newTodo,
      })
      expect(await todoRepository.getAll()).toContainEqual(newTodo)
    })
    it('updates todo.completed and todo.name', async () => {
      await setData(todoRepository, dummyData)
      const requestData = { completed: false, name: 'new name' }
      const newTodo = { id: '2', name: 'new name', completed: false }
      expect(await api.updateTodo('2', requestData)).toEqual({
        status: 'success',
        data: newTodo,
      })
      expect(await todoRepository.getAll()).toContainEqual(newTodo)
    })

    it('returns an error for updating a non-existent todo', async () => {
      expect(await api.updateTodo('0')).toEqual({
        status: 'todoNotFound',
      })
    })

    it('leaves todo unchanged for empty change object', async () => {
      await setData(todoRepository, dummyData)
      expect(await api.updateTodo('2', {})).toEqual({
        status: 'success',
        data: dummyData[2],
      })
      expect(await todoRepository.getAll()).toEqual(dummyData)
    })

    it('leaves todo unchanged if no changes specified', async () => {
      await setData(todoRepository, dummyData)
      expect(await api.updateTodo('2')).toEqual({
        status: 'success',
        data: dummyData[2],
      })
      expect(await todoRepository.getAll()).toEqual(dummyData)
    })
  })
})

const TodoServer = require('./todo-server.js')
const TodoApi = require('./todo-api.js')
const request = require('supertest')
const { CounterIdGenerator } = require('./utils.js')
const InMemoryTodoRepository = require('./repositories/in-memory-todo-repository.js')

const matchers = require('jest-extended')
expect.extend(matchers)

describe('todo-server', () => {
  var server
  var todoRepository
  beforeAll(() => {
    todoRepository = new InMemoryTodoRepository()
    var api = new TodoApi({ idGenerator: CounterIdGenerator(), todoRepository })
    server = new TodoServer(api)
    return server.run(3000)
  })
  afterAll(() => server.stop())

  describe('given the user views the site for the first time', () => {
    it('returns an empty list of todos', async () => {
      const response = await request(server.server)
        .get('/todos')
        .expect('Content-Type', /json/)
        .expect(200)
      expect(response.body.items).toEqual([])
    })
  })

  describe('given the user wants to add a todo', () => {
    it('The todo should be added successfully', () => {
      return request(server.server)
        .post('/todos')
        .send({ name: 'my todo' })
        .expect('Content-Type', /json/)
        .expect(200)
    })
  })

  describe('given the user revisits the todo list after adding', () => {
    it('The todo should be present', async () => {
      const response = await request(server.server)
        .get('/todos')
        .expect('Content-Type', /json/)
        .expect(200)
      expect(response.body.items).toIncludeAllPartialMembers([
        { id: '0', completed: false, name: 'my todo' },
      ])
    })
  })

  describe('given the user marks the todo complete', () => {
    it('The todo should update successfully', async () => {
      const response = await request(server.server)
        .patch('/todos/0')
        .send({ completed: true })
        .expect('Content-Type', /json/)
        .expect(200)
      expect(response.body).toMatchObject({
        id: '0',
        completed: true,
        name: 'my todo',
      })
    })
  })

  describe('given the user revisits the todo list after updating', () => {
    it('The todo should be updated', async () => {
      const response = await request(server.server)
        .get('/todos')
        .expect('Content-Type', /json/)
        .expect(200)
      expect(response.body.items).toIncludeAllPartialMembers([
        { id: '0', completed: true, name: 'my todo' },
      ])
    })
  })

  describe('given the user deletes the todo', () => {
    it('The todo should delete successfully', () => {
      return request(server.server)
        .delete('/todos/0')
        .expect('Content-Type', /json/)
        .expect(200)
    })
  })

  describe('given the user revisits the todo list after deleting', () => {
    it('The todo should be deleted', async () => {
      const response = await request(server.server)
        .get('/todos')
        .expect('Content-Type', /json/)
        .expect(200)
      expect(response.body.items).toEqual([])
    })
  })
})

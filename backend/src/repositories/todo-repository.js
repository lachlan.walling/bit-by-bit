class TodoRepository {
  async setup() {}
  async cleanup() {}

  async getAll() {}
  async getById(id) {}
  async updateTodo(id, changeset) {}
  async save(todo) {}
  async delete(id) {}
}

export default TodoRepository

import mongoose from 'mongoose'
import TodoRepository from './todo-repository'
import { mongooseDeinit, mongooseInit } from './mongoose-init'

class MongooseTodoRepository extends TodoRepository {
  constructor() {
    super()
  }

  async setup() {
    await mongooseInit()
    this.TodoSchema = new mongoose.Schema({
      _id: String,
      name: String,
      completed: Boolean,
    })
    this.TodoModel = mongoose.model('Todo', this.TodoSchema)
  }

  async cleanup() {
    await mongooseDeinit()
  }

  async clearAll() {
    await this.TodoModel.deleteMany({})
    mongoose.deleteModel('Todo')
  }

  convertFromMongo(todo) {
    if (todo) {
      return {
        id: todo._id,
        name: todo.name,
        completed: todo.completed,
      }
    }
    return null
  }

  convertToMongo(todo) {
    if (todo) {
      return {
        _id: todo.id,
        name: todo.name,
        completed: todo.completed,
      }
    }
    return null
  }

  async save(todo) {
    const todoModel = await new this.TodoModel(this.convertToMongo(todo))
    await todoModel.save()
  }

  async getAll() {
    const todos = await this.TodoModel.find()
    return todos.map(this.convertFromMongo)
  }

  async getById(id) {
    const todo = await this.TodoModel.findById(id)
    return this.convertFromMongo(todo)
  }

  async delete(id) {
    await this.TodoModel.findByIdAndDelete(id)
  }

  async updateTodo(id, changeset) {
    return this.TodoModel.findByIdAndUpdate(id, changeset)
  }

  async hasTodo(id) {
    return (await this.getById(id)) != null
  }
}

export default MongooseTodoRepository

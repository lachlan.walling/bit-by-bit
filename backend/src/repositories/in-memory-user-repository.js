import UserRepository from './user-repository'

// TODO should repository return new copies or are references okay?

class InMemoryUserRepository extends UserRepository {
  constructor() {
    super()
    this.users = new Map()
  }

  async getAll() {
    return Array.from(this.users.values())
  }

  async getById(id) {
    return this.users.get(id)
  }

  async save(todo) {
    this.users.set(todo.id, todo)
  }

  async delete(id) {
    this.users.delete(id)
  }

  async findByUsername(username) {
    for (const [_, user] of this.users) {
      if (user.username === username) return user
    }
  }

  async hasUser(id) {
    return this.users.has(id)
  }
}

export default InMemoryUserRepository

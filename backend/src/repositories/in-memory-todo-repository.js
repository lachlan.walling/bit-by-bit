import TodoRepository from './todo-repository'

// TODO should repository return new copies or are references okay?

class InMemoryTodoRepository extends TodoRepository {
  constructor() {
    super()
    this.todos = new Map()
  }

  async getAll() {
    return Array.from(this.todos.values())
  }

  async getById(id) {
    return this.todos.get(id)
  }

  async save(todo) {
    this.todos.set(todo.id, todo)
  }

  async updateTodo(id, changeset) {
    const todo = await this.getById(id)

    if ('completed' in changeset) todo.completed = changeset.completed
    if ('name' in changeset) todo.name = changeset.name
    return todo
  }

  async delete(id) {
    this.todos.delete(id)
  }

  async hasTodo(id) {
    return this.todos.has(id)
  }
}

export default InMemoryTodoRepository

class UserRepository {
  async setup() {}
  async cleanup() {}
  async getAll() {}
  async getById(id) {}
  async findByUsername(username) {}
  async save(todo) {}
  async delete(id) {}
}
export default UserRepository

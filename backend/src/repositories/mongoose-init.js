// TODO replace singleton with something nicer?

import process from 'process'
import mongoose from 'mongoose'

let initialized = false
export const mongooseInit = async () => {
  if (initialized) return
  if (!('BBB_MONGO_URL' in process.env)) {
    throw new Error('Environment variable BBB_MONGO_URL not defined')
  }
  const dbUrl = process.env['BBB_MONGO_URL']
  await mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  initialized = true
}

export const mongooseDeinit = async () => {
  if (!initialized) return
  await mongoose.connection.close()
  initialized = false
}

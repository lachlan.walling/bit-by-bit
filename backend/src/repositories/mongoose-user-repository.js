import mongoose from 'mongoose'
import { mongooseDeinit, mongooseInit } from './mongoose-init'
import UserRepository from './user-repository'

// TODO should repository return new copies or are references okay?

class MongooseUserRepository extends UserRepository {
  constructor() {
    super()
  }

  async setup() {
    await mongooseInit()
    this.UserSchema = new mongoose.Schema({
      _id: String,
      username: String,
      email: String,
      passwordHash: String,
    })
    this.UserModel = mongoose.model('User', this.UserSchema)
  }

  async cleanup() {
    await mongooseDeinit()
  }

  async clearAll() {
    await this.UserModel.deleteMany({})
    mongoose.deleteModel('User')
  }

  convertFromMongo(user) {
    if (user) {
      return {
        id: user._id,
        username: user.username,
        email: user.email,
        passwordHash: user.passwordHash,
      }
    }
    return null
  }

  convertToMongo(user) {
    if (user) {
      return {
        _id: user.id,
        username: user.username,
        email: user.email,
        passwordHash: user.passwordHash,
      }
    }
    return null
  }
  async save(user) {
    const userModel = await new this.UserModel(this.convertToMongo(user))
    await userModel.save()
  }

  async getAll() {
    const users = await this.UserModel.find()
    return users.map(this.convertFromMongo)
  }

  async getById(id) {
    const user = await this.UserModel.findById(id)
    return this.convertFromMongo(user)
  }

  async delete(id) {
    await this.UserModel.findByIdAndDelete(id)
  }

  async findByUsername(username) {
    const user = await this.UserModel.findOne({ username })
    return this.convertFromMongo(user)
  }

  async hasUser(id) {
    return (await this.getById(id)) !== null
  }
}

export default MongooseUserRepository

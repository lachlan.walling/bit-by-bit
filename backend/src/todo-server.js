import express from 'express'
import TodoApi from './todo-api'
import bodyParser from 'body-parser'
import halson from 'halson'
import cors from 'cors'
import sessions from 'client-sessions'

const defaultSessionMiddleware = sessions({
  cookieName: 'session',
  secret: 'TODO put in dotenv',
  duration: 30 * 60 * 1000,
  httpOnly: true,
  //secure: true,
  ephemeral: false,
})

class TodoServer {
  // TODO what should frontendOrigin default to
  constructor(
    api = new TodoApi(),
    frontendOrigin = '',
    sessionMiddleware = defaultSessionMiddleware,
  ) {
    this.api = api
    this.server = express()
    this.server.use(bodyParser.json())
    this.server.use(cors({ origin: frontendOrigin, credentials: true }))

    this.server.use(sessionMiddleware)

    this.server.use(async (req, res, next) => {
      if (req.session && req.session.userId) {
        const response = await api.findUser(req.session.userId)
        if (response.status === 'success') {
          req.user = response.data
        }
      }
      next()
    })

    this.server.get('/healthcheck', async (req, res) => {
      res.status(200).json({})
    })

    const loginRequired = (req, res, next) => {
      if (req.user) next()
      else res.status(403).json({ error: 'Not logged in' })
    }

    this.server.get('/todos', loginRequired, async (req, res) => {
      const apiResponse = await this.api.getTodos()
      if (apiResponse.status === 'success') {
        const todoResources = apiResponse.data.map((todo) =>
          this.createTodoHALResource(todo),
        )
        const responseData = halson({ items: todoResources })
        responseData.addLink('self', '/todos')
        responseData.addLink('create', { href: '/todos', method: 'POST' })
        return res.status(200).json(responseData)
      }

      const response = this.apiResponseToHttp(await this.api.getTodos())
      return res.status(response.statusCode).json(response.data)
    })

    this.server.post('/todos', loginRequired, async (req, res) => {
      const data = req?.body || {}
      const apiResponse = await this.api.addTodo(data.name)
      const response = this.apiTodoResponseToHttp(apiResponse)
      res.status(response.statusCode).json(response.data)
    })

    this.server.get('/todos/:id', loginRequired, async (req, res) => {
      const apiResponse = await this.api.getTodo(req.params.id)
      const response = this.apiTodoResponseToHttp(apiResponse)
      res.status(response.statusCode).json(response.data)
    })

    this.server.delete('/todos/:id', loginRequired, async (req, res) => {
      const apiResponse = await this.api.deleteTodo(req.params.id)
      const response = this.apiResponseToHttp(apiResponse)
      res.status(response.statusCode).json(response.data)
    })

    this.server.patch('/todos/:id', loginRequired, async (req, res) => {
      const apiResponse = await this.api.updateTodo(req.params.id, req.body)
      const response = this.apiTodoResponseToHttp(apiResponse)
      res.status(response.statusCode).json(response.data)
    })

    this.server.post('/register', async (req, res) => {
      const apiResponse = await this.api.register(
        req.body.username,
        req.body.email,
        req.body.password,
      )
      const response = this.apiResponseToHttp(apiResponse)
      if (apiResponse.status === 'success') this.login(req, apiResponse.data.id)
      res.status(response.statusCode).json(response.data)
    })

    this.server.post('/login', async (req, res) => {
      const apiResponse = await this.api.login(
        req.body.username,
        req.body.password,
      )
      const response = this.apiResponseToHttp(apiResponse)
      if (apiResponse.status === 'success') this.login(req, apiResponse.data.id)
      res.status(response.statusCode).json(response.data)
    })

    this.server.post('/logout', async (req, res) => {
      this.logout(req)
      res.status(200).json({})
    })
  }

  logout(req) {
    req.session.userId = undefined
  }

  login(req, userId) {
    req.session.userId = userId
  }

  createTodoHALResource(todo) {
    const todoResource = halson(todo)
    todoResource.addLink('self', `/todos/${todo.id}`)
    todoResource.addLink('collection', '/todos')
    todoResource.addLink('delete', {
      href: `/todos/${todo.id}`,
      method: 'DELETE',
    })
    todoResource.addLink('update', {
      href: `/todos/${todo.id}`,
      method: 'PATCH',
    })
    return todoResource
  }

  apiResponseToHttp(response) {
    const makeErrorResponse = (statusCode, error) => ({
      statusCode,
      data: { error },
    })
    switch (response.status) {
      case 'success':
        return {
          statusCode: 200,
          data: response.data,
        }
      case 'todoNotFound':
        return makeErrorResponse(404, 'Todo not found')
      case 'missingTodoName':
        return makeErrorResponse(400, 'Missing todo name')
      case 'emptyUsername':
        return makeErrorResponse(400, 'Username must not be empty')
      case 'invalidEmail':
        return makeErrorResponse(400, 'Invalid email address')
      case 'emptyPassword':
        return makeErrorResponse(400, 'Password must not be empty')
      case 'badLogin':
        return makeErrorResponse(403, 'Incorrect username or password')
    }
  }

  apiTodoResponseToHttp(apiResponse) {
    if (apiResponse.status === 'success') {
      apiResponse.data = this.createTodoHALResource(apiResponse.data)
    }
    return this.apiResponseToHttp(apiResponse)
  }

  run(port) {
    return new Promise((resolve, reject) => {
      try {
        this.httpServer = this.server.listen(port, () => {
          resolve()
        })
      } catch (err) {
        reject(err)
      }
    })
  }

  stop() {
    return new Promise((resolve, reject) => {
      if (!this.httpServer) resolve()
      try {
        this.httpServer.close(() => {
          this.httpServer = null
          resolve()
        })
      } catch (err) {
        reject(err)
      }
    })
  }
}

export default TodoServer

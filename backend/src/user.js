import bcrypt from 'bcryptjs'

class User {
  constructor(idGenerator, username, email, password) {
    this.id = idGenerator.next()
    this.username = username
    this.email = email
    this.passwordHash = bcrypt.hashSync(password, 14)
  }

  checkPassword(password) {
    return bcrypt.compareSync(password, this.passwordHash)
  }

  getExternalUser() {
    return { id: this.id, username: this.username, email: this.email }
  }
}

export default User

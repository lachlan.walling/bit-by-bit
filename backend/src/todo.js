class Todo {
  constructor(idGenerator, name) {
    this.id = idGenerator.next()
    this.name = name
    this.completed = false
  }
}

export default Todo

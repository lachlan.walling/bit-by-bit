import TodoServer from './todo-server'
import TodoApi from './todo-api'
import process from 'process'
import InMemoryTodoRepository from './repositories/in-memory-todo-repository'
import InMemoryUserRepository from './repositories/in-memory-user-repository'
import MongooseUserRepository from './repositories/mongoose-user-repository'
import MongooseTodoRepository from './repositories/mongoose-todo-repository'
import { DefaultIdGenerator } from './utils'

const repositoryFactories = {
  memory: {
    todoRepository: () => new InMemoryTodoRepository(),
    userRepository: () => new InMemoryUserRepository(),
  },
  mongoose: {
    todoRepository: () => new MongooseTodoRepository(),
    userRepository: () => new MongooseUserRepository(),
  },
}

const selectEnvironmentConfiguration = (envKey, options, defaultOption) => {
  if (envKey in process.env) {
    const requestedOption = process.env[envKey]
    if (requestedOption in options) return options[requestedOption]
    else throw new Error(`Invalid value for ${envKey}: ${requestedOption}`)
  }
  return options[defaultOption]
}

let server
let repositories

const main = async () => {
  // TODO clean up repo when stopping
  let repositoryConfig = selectEnvironmentConfiguration(
    'BBB_REPOSITORY',
    repositoryFactories,
    'memory',
  )
  repositories = {
    todoRepository: repositoryConfig.todoRepository(),
    userRepository: repositoryConfig.userRepository(),
  }
  await repositories.todoRepository.setup()
  await repositories.userRepository.setup()
  const api = new TodoApi({
    createIdGenerator: DefaultIdGenerator,
    ...repositories,
  })
  server = new TodoServer(api, process.env.FRONTEND_ORIGIN)
  server.run(8080)
}

const stop = async () => {
  await server?.stop()
  await repositories?.todoRepository?.cleanup()
  await repositories?.userRepository?.cleanup()
}

process.on('SIGINT', stop)
process.on('SIGTERM', stop)

main()

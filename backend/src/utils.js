import { v4 as uuidv4 } from 'uuid'

export const DefaultIdGenerator = () => ({
  next() {
    return uuidv4()
  },
})

export const CounterIdGenerator = () => ({
  id: 0,
  next() {
    return String(this.id++)
  },
})

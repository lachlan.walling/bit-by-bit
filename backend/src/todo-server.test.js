import TodoServer from './todo-server'
import TodoApi from './todo-api'
import request from 'supertest'
import { CounterIdGenerator } from './utils'
import InMemoryTodoRepository from './repositories/in-memory-todo-repository'
import InMemoryUserRepository from './repositories/in-memory-user-repository'
import * as matchers from 'jest-extended'
import User from './user'
import TestSession from './test/test-session'

expect.extend(matchers)

// TODO expect in beforeEach?

const dummyData = [
  { id: '0', name: 'todo 1', completed: true },
  { id: '1', name: 'todo 2', completed: false },
  { id: '2', name: 'todo 3', completed: true },
  { id: '3', name: 'todo 4', completed: false },
]

const setData = (repository, data) => {
  data.forEach(async (todo) => await repository.save(structuredClone(todo)))
}

describe('todo-server', () => {
  var server
  var todoRepository
  var userRepository
  var session
  beforeEach(() => {
    todoRepository = new InMemoryTodoRepository()
    userRepository = new InMemoryUserRepository()
    var api = new TodoApi({
      createIdGenerator: CounterIdGenerator,
      todoRepository,
      userRepository,
    })
    session = new TestSession()
    server = new TodoServer(api, '', session.middleware)
    return server.run(3001)
  })
  afterEach(() => server.stop())

  describe('routing errors', () => {
    it('returns a 404 on an invalid path', () => {
      return request(server.server).get('/invalid').expect(404)
    })

    describe('todo routes', () => {
      it('GET /todos returns a 403 when not logged in', () => {
        return request(server.server).get('/todos').expect(403)
      })
      it('POST /todos returns a 403 when not logged in', () => {
        return request(server.server).post('/todos').expect(403)
      })
      it('GET /todos/:id returns a 403 when not logged in', () => {
        return request(server.server).get('/todos/0').expect(403)
      })
      it('DELETE /todos/:id returns a 403 when not logged in', () => {
        return request(server.server).delete('/todos/0').expect(403)
      })
      it('PATCH /todos/:id returns a 403 when not logged in', () => {
        return request(server.server).patch('/todos/0').expect(403)
      })
    })
  })

  describe('registration', () => {
    it('returns an error with no username', async () => {
      const requestData = {
        username: '',
        password: 'god',
        email: 'someuser@somesite.com',
      }
      await request(server.server)
        .post('/register')
        .send(requestData)
        .expect(400, {
          error: 'Username must not be empty',
        })
    })
    it('returns an error with no password', async () => {
      const requestData = {
        username: 'username',
        password: '',
        email: 'someuser@somesite.com',
      }
      await request(server.server)
        .post('/register')
        .send(requestData)
        .expect(400, {
          error: 'Password must not be empty',
        })
    })
    it('returns an error with no email', async () => {
      const requestData = { username: 'username', password: 'god', email: '' }
      await request(server.server)
        .post('/register')
        .send(requestData)
        .expect(400, {
          error: 'Invalid email address',
        })
    })
    it('returns an error with an invalid email', async () => {
      const requestData = {
        username: 'username',
        password: 'god',
        email: 'aaaaa',
      }
      await request(server.server)
        .post('/register')
        .send(requestData)
        .expect(400, {
          error: 'Invalid email address',
        })
    })
    it('registers a user', async () => {
      const requestData = {
        username: 'username',
        password: 'god',
        email: 'someuser@somesite.com',
      }
      await request(server.server)
        .post('/register')
        .send(requestData)
        .expect('Set-Cookie', /.*/)
        .expect(200, {
          id: '0',
          username: 'username',
          email: 'someuser@somesite.com',
        })
    })
  })

  describe('login', () => {
    let user
    beforeEach(async () => {
      user = new User(
        CounterIdGenerator(),
        'username',
        'someuser@somesite.com',
        'god',
      )
      await userRepository.save(user)
    })
    it('given an empty username it returns an error', async () => {
      const requestData = { username: '', password: 'god' }
      await request(server.server)
        .post('/login')
        .send(requestData)
        .expect(400, {
          error: 'Username must not be empty',
        })
    })
    it('given an empty password it returns an error', async () => {
      const requestData = { username: 'username', password: '' }
      await request(server.server)
        .post('/login')
        .send(requestData)
        .expect(400, {
          error: 'Password must not be empty',
        })
    })
    it('given an invalid user it returns an error', async () => {
      const requestData = { username: 'nonexistent', password: 'god' }
      await request(server.server)
        .post('/login')
        .send(requestData)
        .expect(403, {
          error: 'Incorrect username or password',
        })
    })
    it('given an incorrect password it returns an error', async () => {
      const requestData = { username: 'username', password: 'wrong' }
      await request(server.server)
        .post('/login')
        .send(requestData)
        .expect(403, {
          error: 'Incorrect username or password',
        })
    })
    it('logs in a user', async () => {
      const requestData = { username: 'username', password: 'god' }
      await request(server.server)
        .post('/login')
        .send(requestData)
        .expect('Set-Cookie', /.*/)
        .expect(200, {
          id: '0',
          username: 'username',
          email: 'someuser@somesite.com',
        })
    })
  })

  describe('logout', () => {
    it('logs out the user', async () => {
      const user = new User(
        CounterIdGenerator(),
        'someuser',
        'someuser@somesite.com',
        'god',
      )
      userRepository.save(user)
      session.logInTest(user.id)
      await request(server.server)
        .post('/logout')
        .expect('Set-Cookie', /.*/)
        .expect(200)
      expect(session.isLoggedIn()).not.toBeTruthy()
    })
  })

  describe('todos', () => {
    beforeEach(() => {
      const user = new User(
        CounterIdGenerator(),
        'someuser',
        'someuser@somesite.com',
        'god',
      )
      userRepository.save(user)
      session.logInTest(user.id)
    })

    describe('getting todos', () => {
      describe('without data', () => {
        let todosResponse
        beforeEach(async () => {
          todosResponse = await request(server.server).get('/todos').expect(200)
        })
        it('gets an empty list of todos', async () => {
          expect(todosResponse.body.items).toEqual([])
        })

        it('returns a HAL resource with a self link', async () => {
          expect(todosResponse.body._links.self.href).toEqual('/todos')
        })
      })

      describe('with data', () => {
        let todosResponse
        beforeEach(async () => {
          setData(todoRepository, dummyData)
          todosResponse = await request(server.server).get('/todos').expect(200)
        })

        it('gets a list of todos', () => {
          expect(todosResponse.body.items).toIncludeAllPartialMembers(dummyData)
        })

        it('includes all todos in the resource as items with self links', () => {
          expect(todosResponse.body.items[0]._links.self.href).toEqual(
            `/todos/${todosResponse.body.items[0].id}`,
          )
        })

        it('includes a create link', () => {
          expect(todosResponse.body._links.create).toEqual({
            href: '/todos',
            method: 'POST',
          })
        })
      })
    })

    describe('getting a particular todo', () => {
      let todosResponse
      beforeEach(async () => {
        setData(todoRepository, dummyData)
        todosResponse = await request(server.server).get('/todos/2').expect(200)
      })
      it('gets a particular todo', async () => {
        expect(todosResponse.body).toMatchObject({
          id: '2',
          name: 'todo 3',
          completed: true,
        })
      })

      it('includes a delete link', () => {
        const id = todosResponse.body.id
        expect(todosResponse.body._links.delete).toEqual({
          href: `/todos/${id}`,
          method: 'DELETE',
        })
      })

      it('includes an update link', () => {
        const id = todosResponse.body.id
        expect(todosResponse.body._links.update).toEqual({
          href: `/todos/${id}`,
          method: 'PATCH',
        })
      })
    })

    describe('adding todo', () => {
      it('returns an error with no data', () => {
        return request(server.server)
          .post('/todos')
          .expect(400, { error: 'Missing todo name' })
      })
      it('adds a todo', async () => {
        const requestData = { name: 'todo' }
        const response = await request(server.server)
          .post('/todos')
          .send(requestData)
          .expect(200)

        expect(response.body).toMatchObject({
          id: '0',
          name: 'todo',
          completed: false,
        })

        expect(await todoRepository.getAll()).toEqual([
          { id: '0', name: 'todo', completed: false },
        ])
      })
    })

    describe('deleting todo', () => {
      it('deletes a todo', async () => {
        setData(todoRepository, dummyData)
        await request(server.server).delete('/todos/2').expect(200)
        expect(await todoRepository.getAll()).toEqual(
          dummyData.filter((todo) => todo.id !== '2'),
        )
      })
    })

    describe('update todo', () => {
      it('updates todo.completed', async () => {
        setData(todoRepository, dummyData)
        const requestData = { completed: false }
        const newTodo = { id: '2', name: 'todo 3', completed: false }
        const response = await request(server.server)
          .patch('/todos/2')
          .send(requestData)
          .expect(200)

        expect(response.body).toMatchObject(newTodo)
        expect(await todoRepository.getById('2')).toEqual(newTodo)
      })

      it('updates todo.name', async () => {
        setData(todoRepository, dummyData)
        const requestData = { name: 'new name' }
        const newTodo = { id: '2', name: 'new name', completed: true }
        const response = await request(server.server)
          .patch('/todos/2')
          .send(requestData)
          .expect(200)

        expect(response.body).toMatchObject(newTodo)
        expect(await todoRepository.getById('2')).toEqual(newTodo)
      })

      it('updates todo.completed and todo.name', async () => {
        setData(todoRepository, dummyData)
        const requestData = { completed: false, name: 'new name' }
        const newTodo = { id: '2', name: 'new name', completed: false }
        const response = await request(server.server)
          .patch('/todos/2')
          .send(requestData)
          .expect(200)

        expect(response.body).toMatchObject(newTodo)
        expect(await todoRepository.getById('2')).toEqual(newTodo)
      })
    })
  })

  describe('status codes', () => {
    it('maps success to 200', () => {
      expect(server.apiResponseToHttp({ status: 'success', data: {} })).toEqual(
        {
          statusCode: 200,
          data: {},
        },
      )
    })
    it('maps unknown todos to 404', () => {
      expect(server.apiResponseToHttp({ status: 'todoNotFound' })).toEqual({
        statusCode: 404,
        data: {
          error: 'Todo not found',
        },
      })
    })
    it('maps missing todo name to 400', () => {
      expect(server.apiResponseToHttp({ status: 'missingTodoName' })).toEqual({
        statusCode: 400,
        data: {
          error: 'Missing todo name',
        },
      })
    })
  })
})

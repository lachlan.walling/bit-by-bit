const { merge } = require('webpack-merge')
const NodemonPlugin = require('nodemon-webpack-plugin')
const WebpackCommon = require('./webpack.common')

module.exports = merge(WebpackCommon, {
  mode: 'development',
  plugins: [new NodemonPlugin()],
})
